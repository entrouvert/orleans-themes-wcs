/* IE8 */
if(!document.getElementsByClassName) {
    document.getElementsByClassName = function(className) {
        return this.querySelectorAll("." + className);
    };
    Element.prototype.getElementsByClassName = document.getElementsByClassName;
}

/* dirty hack pour éviter le double-clic sur un submit */

var submit_buttons = document.getElementsByClassName('submit-button')[0];
if (submit_buttons) {
    var submit_button = submit_buttons.childNodes[0].childNodes[0];
    submit_button.form.onsubmit = function() {
        setTimeout(function() {submit_button.disabled = 'disabled'}, 0);
        return true;
    }
}

/* les input de class "wcs-password" sont forcés en type "password" */

var wcs_passwords = document.getElementsByClassName('wcs-password');
if (wcs_passwords) {
    for(var i=0; i<wcs_passwords.length; i++) {
        input = wcs_passwords[i].getElementsByTagName('input')[0];
        try {
            input.type = 'password';
        } catch (e) { /* IE => on remplace le input complet */ 
	    var inp = document.createElement('input');
            inp.type='password';
            var attributes = ['id', 'name', 'size'];
            for(i=0;i<attributes.length;i++){
                inp[attributes[i]] = input[attributes[i]];
            }
            var par = input.parentNode;
            par.insertBefore(inp,input);
            par.removeChild(input);
	}
    }
}

